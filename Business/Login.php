<?php
require_once ("./Database/shUserDB.php");
class Login extends Common
{
	
	private $userName ="";
	private $password ="";
	public $loginObject;
	public $userInfo;
	
	function __construct()
	{
		$this->userDB = new ShUserDB();
	}
	
	public function Login($userName, $password)
	{
		self::logError("userName:::".$userName."::password::".$password);
		$this->userName = trim($userName);
		$this->password = trim($password);
		$valRslt = $this->validate();
		
		if(self::isError($valRslt))
		{
			return $valRslt->error;
		}
		if(self::isSessionValid($this->userName))
		{
			$this->userInfo =  self::getUserInfo();
			if(self::isError($this->userInfo))
			{
				self::unsetSession();
				return $this->userInfo->error;
			}
			return $this->userInfo;
		}
  		$authRslt = self::authenticate();
		if(self::isError($authRslt))
		{
			return $authRslt->error;
		}
		else
		{
			self::setSession();
		}
		return $this->userInfo;
	}
	
  	private function validate()
  	{
  		if(empty($this->userName))
		{
			return new ErrorMessage("10001");
		}
				
		if(empty($this->password))
		{
			return new ErrorMessage("10002");
		}
		$valRslt = self::validateEmail($this->userName);
		if(self::isError($valRslt))
		{
			return $valRslt;
		}
  	}
  	
	private function authenticate()
	{
		$info = self::getUserInfo();
		self::logError("UserInfo::".print_r($info,1));
		if(self::isError($info))
		{
			return $info;
		}
		$this->userInfo =$info;
		$obj = $this->validatePassword($this->password, $info->getPassword());
		if(self::isError($obj))
		{
			return $obj;
		}
		return;
	}
	
	private function validatePassword($passwd, $storedPasswd)
	{
		if(self::encryptPassword($passwd) === $storedPasswd)
			return true;
		else
			return new ErrorMessage("10006");
	}
	
	private function getSessionExpireTime()
	{
		$expireTime = time() + SESSIONEXPIRY;
		return $expireTime; 
	}
	
	private function getUserInfo()
	{
		$info = $this->userDB->getUserInfo($this->userName);
		if(empty($info) || count($info) == 0 || count($info) > 1)
		{
			return new ErrorMessage("10001");
		}
		return $info[0];
	}
	
	private function clearSession()
	{
		self::unSetSession();
	}
	
	private function setSession()
	{
		$_SESSION['UUID'] = self::createGuid($this->userInfo->getUserName());
		$_SESSION['firstName']  = $this->userInfo->getFirstName();
		$_SESSION['lastName']  = $this->userInfo->getLastName();
		$_SESSION['userName']  = $this->userInfo->getUserName();
		$_SESSION['userId']  = $this->userInfo->getUserId();
		$_SESSION['sessionExpire']= self:: getSessionExpireTime();
	}
	
	public function logOut()
	{
		self::clearSession();
		return true;
	}
	
	
}
