<?php
require_once './Common/ErrorMessage.php';
require_once ("./conf/config.php");
require_once ("./Database/shUserDB.php");
require_once ("./Business/Login.php");
class Register extends Common
{
	private $firstName;
	private $lastName;
	private $userName;
	private $password;
	private $confPassword;
	public function __construct($firstName, $lastName, $userName, $password, $confPassword)
	{
		$this->firstName = $firstName;	
		$this->lastName = $lastName;	
		$this->userName = $userName;	
		$this->password = $password;	
		$this->confPassword = $confPassword;
		$this->userDB = new ShUserDB();
	}
	
	public function Register()
	{
		$obj = $this->validateFields();
		if(self::isError($obj))
		{
			return $obj->error;
		}
		$this->userDB->insertUser($this->firstName,$this->lastName, $this->userName, self::encryptPassword($this->password));
		$login  =  new Login();
		$info =$login->Login($this->userName,$this->password);
		return $info;
	}
	
	private function validateFields()
	{
		if(empty($this->firstName))
		{
			return new ErrorMessage("10007");
		}
		
		if(strlen($this->firstName)>60)
		{
			return new ErrorMessage("10010");
		}
		
		if(empty($this->lastName))
		{
			return new ErrorMessage("10008");
		}
		
		if(strlen($this->lastName)>60)
		{
			return new ErrorMessage("10011");
		}
		
		if(empty($this->userName))
		{
			return new ErrorMessage("10001");
		}

		$valRst  = self::validateEmail($this->userName);
		if(self::isError($valRst))
		{
			return $valRst;
		}
		
		if(!($this->password === $this->confPassword))
		{
			return new ErrorMessage("10009");
		}
		
		$info = $this->userDB->getUserInfo($this->userName);
		if(!empty($info))
		{
			return new ErrorMessage("10013");
		}
		
	}
}
