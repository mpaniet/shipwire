<?php
require_once ("./Database/shOrderDB.php");
class Orders extends Common
{
	public function __construct()
	{
		self::logError("Inside orders...");
		$this->orderDB = new ShOrderDB();
	}
	
	public function saveOrder($params)
	{
		self::logError("Inside saveOrder...::".print_r($params,1));
		if(empty($params))
		{
			self::logError("Unable to save order!");
			return new ErrorMessage("10016");
		}
		else
		{
			return $this->orderDB->saveOrder($params['userId'],$params['prodId'], $params['rcptName'], 
				$params['strtAddress'], $params['city'], $params['state'], $params['quantity'], $params['phone'], $params['price'], $params['zip']);
		}
	}
	
	public function getTotalProducts($userId)
	{
		self::logError("Inside getTotalProducts...userId::".$userId);
		if(empty($userId))
		{
			self::logError("Please login to view your products!!");
			$params['action']= "logout";
			self::processRequest($params);
			return new ErrorMessage("10015");
		}
		else
		{
			$count = $this->productDB->getProductsCount($userId);
		}
	}
	
	public function getNumPages($userId)
	{
		self::logError("Inside getTotalProducts...userId::".$userId);
		if(empty($userId))
		{
			self::logError("Please login to view your products!!");
			$params['action']= "logout";
			self::processRequest($params);
			return new ErrorMessage("10015");
		}
		else
		{
			$numPages = ceil(self::getTotalProducts($userId)/LIMIT_PER_PAGE);
			return $numPages;
		}
	}
	
	public function getProductInfo($prodId)
	{
		self::logError("Inside getProductInfo...prodId::".$prodId);
		if(empty($prodId))
		{
			return null;
		}
		else
		{
			return $this->productDB->getProductInfo($prodId);
		}
	}
}