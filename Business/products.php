<?php
require_once ("./Database/shProductDB.php");
class Products extends Common
{
	public function __construct()
	{
		self::logError("Inside products...");
		$this->productDB = new ShProductDB();
	}
	
	public function getProducts($userId, $page)
	{
		self::logError("Inside getProducts...userId::".$userId);
		if(empty($userId))
		{
			self::logError("Please login to view your products!!");
			$params['action']= "logout";
			self::processRequest($params);
			return new ErrorMessage("10015");
		}
		else
		{
			return $this->productDB->getAllProducts($userId, LIMIT_PER_PAGE, $page);
		}
	}
	
	public function getTotalProducts($userId)
	{
		self::logError("Inside getTotalProducts...userId::".$userId);
		if(empty($userId))
		{
			self::logError("Please login to view your products!!");
			$params['action']= "logout";
			self::processRequest($params);
			return new ErrorMessage("10015");
		}
		else
		{
			$count = $this->productDB->getProductsCount($userId);
		}
	}
	
	public function getNumPages($userId)
	{
		self::logError("Inside getTotalProducts...userId::".$userId);
		if(empty($userId))
		{
			self::logError("Please login to view your products!!");
			$params['action']= "logout";
			self::processRequest($params);
			return new ErrorMessage("10015");
		}
		else
		{
			$numPages = ceil(self::getTotalProducts($userId)/LIMIT_PER_PAGE);
			return $numPages;
		}
	}
	
	public function getProductInfo($prodId)
	{
		self::logError("Inside getProductInfo...prodId::".$prodId);
		if(empty($prodId))
		{
			return null;
		}
		else
		{
			return $this->productDB->getProductInfo($prodId);
		}
	}
	
	public function delProduct($prodId)
	{
		self::logError("Inside delProduct...prodId::".$prodId);
		if(empty($prodId))
		{
			return null;
		}
		else
		{
			return $this->productDB->delProduct($prodId);
		}
	}
	
	public function getAllCurrency()
	{
		self::logError("Inside getAllCurrency...:");
		return $this->productDB->getCurrency();
	}
	
	public function insertProduct($params)
	{
		self::logError("Inside insertProduct...:".print_r($params,1));
		if(isset($params['currency']))
			$currency= $params['currency'];
		else 
			$currency=null;
		$this->productDB->insertProducts($params['userId'],$params['prodName'],$params['prodDesc'], $params['width'], $params['length'],$params['height'],$params['weight'],$currency,$params['qty']);
		return null;
	}
	
	public function updateProduct($params)
	{
		self::logError("Inside updateProduct...:".print_r($params,1));
		$prodId = $this->productDB->updateProduct($params['prodId'], $params['userId'],stripslashes($params['prodName']),stripslashes($params['prodDesc']), $params['width'], $params['length'],$params['height'],$params['weight'],$params['currency'],$params['qty']);
		return null;
	}
	
	public function  insertAllProduct($params)
	{
		self::logError("Inside insertAllProduct...:".print_r($params,1));
		for($i=0;$i< count($params['info']);$i++)
		{
			$params['info'][$i]['prodName']=str_replace("#39;","'",$params['info'][$i]['prodName']);	
			$params['info'][$i]['prodDesc']=str_replace("#39;","'",$params['info'][$i]['prodDesc']);	
			$params['info'][$i]['userId'] = $params['userId'];
			self::insertProduct($params['info'][$i]);
		}
		return null;
	}
}