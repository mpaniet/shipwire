<?php

class ErrorMessage
{
	function __construct($errorCode)
	{
		$this->error= null; 
		if(!empty($errorCode))
		 	$this->error = self::execute($errorCode);
		 	
	}
	
	private function execute($errCode)
	{
		$errArr = array("errorCode"=>$errCode);
		foreach(self::errorMessageWithCodes() as $errorCode =>$errorMessage)
		{
			if(trim($errCode)==trim($errorCode))
			{
				$errArr["errorMessage"] = $errorMessage;
				break;
			}
		}
		return $errArr;
	}
	
	private function errorMessageWithCodes()
	{
		$errorMessageArray =
		Array(
			"10001"=>"Please provide a valid userName!!",
			"10002"=>"Please provide a valid password!!",
			"10003"=>"An Error occurred while connecting to database!!",
			"10004"=>"Unable to connect to database!!",
			"10005"=>"Please provide a valid email address!!",
			"10006"=>"Please provide a valid password!!",
			"10007"=>"Please provide a valid first name!!",
			"10008"=>"Please provide a valid last name!!",
			"10009"=>"Invalid Password!!",
			"10010"=>"Maximum allowed characters for first name is 60!!",
			"10011"=>"Maximum allowed characters for last name is 60!!",
			"10012"=>"Unable to get info!!",
			"10013"=>"User already exists!!",
			"10014"=>"Please login again!!",
			"10015"=>"Please login to view your products!!",
			"10016"=>"Unable to save order, please provide valid inputs!!",
		);
		return $errorMessageArray;
	}
}
//Unit Test
/*
 * $errorTest  = new ErrorMessage("");
 * echo $errorTest->error;
 * 
 */

