<?php
require_once ("./Business/Register.php");
require_once ("./Business/products.php");
require_once ("./Business/orders.php");

class Common
{
	protected function isError($obj)
	{
		if(isset($obj) && isset($obj->error) && !empty($obj->error))
			return true;
		else 
			return false;	
	}
	
	protected function createGuid($userName="") 
	{    
    	static $guid = '';
    	$hash = strtoupper(hash('ripemd128', $guid . md5($userName)));
	    $guid = '{' .  
	            substr($hash,  0,  8) .
	            '-' .
	            substr($hash,  8,  4) .
	            '-' .
	            substr($hash, 12,  4) .
	            '-' .
	            substr($hash, 16,  4) .
	            '-' .
	            substr($hash, 20, 12) .
	            '}';
    	return $guid;
  	}
  	
	protected function getSalt() {
    	$salt = sprintf('$2a$%02d$', PASS_CYCLE);
    	return $salt;
	}
	
	protected function validateEmail($userName)
	{
		$sntizedEmail = filter_var($userName, FILTER_SANITIZE_EMAIL);
		if (!filter_var($sntizedEmail, FILTER_VALIDATE_EMAIL)) {
			return  new ErrorMessage("10005");
		}
		return;
	}
	
	protected function encryptPassword($pass)
	{
		return crypt($pass, self::getSalt());
	}
	
	protected function isSessionValid($userName)
	{
		if(isset($_SESSION) && isset($_SESSION['UUID']))
		{
			$getGUID = self::createGuid($userName);
			if($_SESSION['UUID'] === $getGUID)
			{
				
				if($_SESSION['sessionExpire'] > time())
				{
					return true;
				}
			}
			else
			{
				self::unSetSession();
				return false;
			}
		}
		return false;
	}
	
	public function logError($msg)
	{
		$date = "[".date("Y-m-d H:i:s")."]";
		$msg =$date." ".$msg."\n"; 
		error_log($msg, 3,"./Log/Shipwire.log");
	}
	
	public function processRequest($params)
	{
		self::logError("Inside processRequest::Params::".print_r($params,1));
		if(isset($params['action']))
		{
			switch (trim($params['action']))
			{
				case "register":
					$reg =  new Register($params['firstName'],$params['lastName'],$params['userName'], $params['password'], $params['confPassword']);
					$response = $reg->Register();
					break; 
			 	case "logout":
					$logIn =  new Login();
					$response = $logIn->logOut();
					break; 
				case "login":
					$logIn =  new Login();
					$response = $logIn->Login($params['userName'], $params['password']);
					break; 	
				case "getProducts":
					$products =  new Products();
					$response = $products->getProducts($params['userId'], $params['page']);
					break;
				case "getProductDetailInfo":
					$products =  new Products();
					$response = $products->getProductInfo($params['prodId']);
					break;	
				case "placeOrder":
					$orders =  new Orders();
					$response = $orders->saveOrder($params);
					break;	
				case "delProduct":	
					$products =  new Products();
					$response = $products->delProduct($params['prodId']);
					break;
				case "getCurrency":	
					$products =  new Products();
					$response = $products->getAllCurrency();
					break;
				case "insertProduct":	
					$products =  new Products();
					$response = $products->insertProduct($params);
					break;
				case "updateProduct":	
					$products =  new Products();
					$response = $products->updateProduct($params);
					break;
				case "insertCsvProduct":
					$products =  new Products();
					$response = $products->insertAllProduct($params);
					break;				
			}
		}
		self::logError("Inside processRequest::Response::".print_r($response,1));
		return $response;
	}
	
	protected  function unSetSession()
	{
		unset($_SESSION['UUID']);
		unset($_SESSION['sessionExpire']);
		unset($_SESSION['firstName']);
		unset($_SESSION['lastName']);
		unset($_SESSION['userName']);
		unset($_SESSION['userId']);
	}
}