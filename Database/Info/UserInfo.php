<?php
class UserInfo
{
	public function __construct($userId, $userName, $password, $firstName, $lastName)
	{
		$this->userId =$userId;
		$this->userName =$userName;
		$this->password =$password;
		$this->firstName =$firstName;
		$this->lastName =$lastName;
	}
	
	public function getUserId()
	{
		return $this->userId;
	}
	
	public function getUserName()
	{
		return $this->userName;
	}
	
	public function getPassword()
	{
		return $this->password;
	}
	
	public function getFirstName()
	{
		return $this->firstName;	
	}
	
	public function getLastName()
	{
		return $this->lastName;	
	}
}