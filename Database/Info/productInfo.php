<?php
class ProductInfo
{
	
	private $productId;
	private	$userId;
	private $productName;
	private $productDesc;
	private $width;
	private $length;
	private $weight;
	private $value;
	private $currency;
	private $quantity;
	public function __construct($prodId, $userId, $name, $description, $width, $length, $height, $weight, $value, $currency, $quantity)
	{
		
		$this->productId = $prodId;
		$this->userId = $prodId;
		$this->productName = $name;
		$this->productDesc = $description;
		$this->width = $width;
		$this->length = $length;
		$this->weight = $weight;
		$this->value = $value;
		$this->currency = $currency;
		$this->quantity = $quantity;
	}
	
	public function getProductId()
	{
		return 	$this->productId;
	}
	
	public function getUserId()
	{
		return 	$this->userId;
	}
	
	public function getProductName()
	{
		return 	$this->productName;
	}
	
	public function getProductDesc()
	{
		return 	$this->productDesc;
	}
	
	public function getWidth()
	{
		return 	$this->width;
	}
	
	public function getLength()
	{
		return 	$this->length;
	}
	
	public function getWeight()
	{
		return 	$this->weight;
	}
	
	public function getValue()
	{
		return 	$this->value;
	}
		
	public function getCurrency()
	{
		return 	$this->currency;
	}
	
	public function getQuantity()
	{
		return 	$this->quantity;
	}
	
	
}