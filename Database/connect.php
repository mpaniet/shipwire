<?php
include_once 'DB.php';
require_once './conf/config.php';

class Connect 
{
	public static function get_connection()
	{
		$db =&DB::connect(DSN, FALSE);
		if (PEAR::isError($db)) {
			$msg = 'Could not connect to DB: ' .
				'. Debug info: ' . $db->getDebugInfo() . "\n";
			return new ErrorMessage("10004");
		}
		return $db;
	}
}
/* Unit Test */
/* $cnn = new connect();
 * $conn = $cnn->get_connection(DSN); 
 */

