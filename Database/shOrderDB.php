<?php 
require_once ("connect.php");
require_once ("Info/productInfo.php");
require_once ("Common/common.php");

class ShOrderDB extends Common
{
	public function  __construct()
	{
		$this->connection=Connect::get_connection();
	}
	
	public function  __destruct()
	{
		if($this->connection){
        	$this->connection->disconnect();
      	}
	}
	
	public function saveOrder($userId,$prodId, $rcptName, $strtAddress, $city, $state, $qty, $uphone, $price, $zip)
	{
		self::logError("Inside ShOrderDB saveOrder...::".$userId,$prodId, $rcptName, $strtAddress, $city, $state, $qty, $uphone, $price, $zip);
		$sql = "INSERT INTO sh_orders(`recipientName`,`productId`,`userId`,`streetAdress`,`city`,`state`,`zipCode`,`phoneNumber`,`quantity`,`price`, `createdOn`)VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		$values = array($rcptName, $prodId, $userId, $strtAddress, $city, $state, $zip, $uphone, $qty, $price, date("Y-m-d H:i:s"));
		$result = $this->connection->query($sql, $values);
		$info = array();
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		$this->connection->execute($result);
		return true;
	}
}
