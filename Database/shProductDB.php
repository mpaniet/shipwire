<?php 
require_once ("connect.php");
require_once ("Info/productInfo.php");
require_once ("Common/common.php");

class ShProductDB extends Common
{
	public function  __construct()
	{
		$this->connection=Connect::get_connection();
	}
	
	public function  __destruct()
	{
		if($this->connection){
        	$this->connection->disconnect();
      	}
	}
	
	public function getAllProducts($userId, $limit, $page)
	{
		$sql = "SELECT prodId, userId, name, description, width, length, height, weight, quantity FROM sh_products WHERE userId =? AND status='A'";
		if(!empty($page) && !empty($limit))
		{
			$sql .= " LIMIT $page, $limit";
		}
		$result = $this->connection->query($sql, array($userId));
		$info = array();
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		if($result->numRows()==0)
			return null;
				
		while($result->fetchInto($prodRow, DB_FETCHMODE_ASSOC))
		{
				$prodRow['currencyInfo'] = self::getProdCurrency($prodRow['prodId']);
				array_push($info, $prodRow);
		}
		return $info;
	}
	
	public function insertProducts($userId, $name, $description, $width, $length, $height, $weight, $currency, $quantity)
	{
		$sql ="INSERT INTO sh_products(`userId`, `name`, `description`, `width`, `length`, `height`, `weight`, `quantity`, `createdOn`) VALUES (?,?,?,?,?,?,?,?,?)";
		$values = array($userId, $name, $description, $width, $length, $height, $weight, $quantity, date("Y-m-d H:i:s"));
		$result = $this->connection->prepare($sql);
		$this->connection->execute($result,$values);
		$prodId= mysql_insert_id($this->connection->connection);
		if(!is_null($currency))
		{
			foreach($currency as $key => $value)
			{
				self::insertProdCurrency($prodId, self::getCurrencyIdByCode($key), $value);
			}
		}
		return true;
	}
	
	public function getProductCount($userId)
	{
		$sql = "SELECT prodId FROM sh_products WHERE userId =?";
		$values = array($userId);
		$result = $this->connection->query($sql,$values);
		return $result->numRows();
	}
	
	public function getProductInfo($prodId)
	{
		if(empty($prodId))
			return null;
		$sql = "SELECT prodId, userId, name, description, width, length, height, weight,  quantity FROM sh_products WHERE  prodId =? AND status='A'";
		$result = $this->connection->query($sql, array($prodId));
		$info = array();
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		if($result->numRows()==0)
			return null;
		$result->fetchInto($prodRow, DB_FETCHMODE_ASSOC);
		$prodRow['currencyInfo'] = self::getProdCurrency($prodRow['prodId']);
		return $prodRow;		
	}
	
	public function delProduct($prodId)
	{
		if(empty($prodId))
			return null;
			
		$sql ="UPDATE sh_products SET status=?, updatedOn=?  WHERE prodId=?";
		$values = array('I', date("Y-m-d H:i:s"),$prodId);
		$result = $this->connection->query($sql,$values);
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		$this->connection->execute($result);
		
		$sql = "UPDATE sh_prod_currency SET status=?, `updatedOn` =? WHERE status=? AND prodId= ?";
		$values = array("I", date("Y-m-d H:i:s"), "A", $prodId); 
		$result = $this->connection->prepare($sql);
		$this->connection->execute($result,$values);
		return true;
	}
	
	public function getCurrency()
	{
		$sql="SELECT `currencyId`, `country`, `currencyCode`, `symbol` FROM sh_currency";
		$result = $this->connection->query($sql);
		$info = array();
		if($result->numRows()==0)
			return null;
		while($result->fetchInto($currRow, DB_FETCHMODE_ASSOC))
		{
				array_push($info, $currRow);
		}
		return $info;	
	}
	
	public function insertProdCurrency($prodId, $currencyId, $value)
	{
		$sql ="INSERT INTO sh_prod_currency (`prodId`, `currencyId`, `value`, `createdOn`) VALUES(?, ?, ?, ?)";
		$values = array($prodId, $currencyId, $value, date("Y-m-d H:i:s"));
		$result = $this->connection->query($sql,$values);
	}
	
	public function getCurrencyIdByCode($currencyCode)
	{
		$sql ="SELECT currencyId FROM sh_currency WHERE  currencyCode = ?";
		$values = array($currencyCode);
		$result = $this->connection->query($sql,$values);
		$result->fetchInto($currRow, DB_FETCHMODE_ASSOC);
		return $currRow['currencyId'];
	}
	
	public function getProdCurrency($prodId)
	{
		$sql ="SELECT  sh_currency.currencyId as currencyId, `value`, `symbol`, `currencyCode`  FROM sh_prod_currency, sh_currency WHERE sh_prod_currency.currencyId = sh_currency.currencyId AND prodId=? AND status='A'";
		$values = array($prodId);
		$currInfo = array();
		$result = $this->connection->query($sql, $values);
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		if($result->numRows()==0)
			return null;
		while($result->fetchInto($currRow, DB_FETCHMODE_ASSOC))
		{
				array_push($currInfo, $currRow);
		}
		return 	$currInfo;
	}
	
	public function updateProduct($prodId, $userId, $name, $description, $width, $length, $height, $weight, $currency, $quantity)
	{
		$sql = "UPDATE sh_products SET `userId` =?, `name`=?, `description`=?, `width` =? , `length` =?, `height` =?, `weight` =?, `quantity` =?, `updatedOn` =? WHERE prodId= ?";
		$values = array($userId, $name, $description, $width, $length, $height, $weight, $quantity, date("Y-m-d H:i:s"),$prodId);
		$result = $this->connection->prepare($sql);
		$this->connection->execute($result,$values);
		
		$sql = "UPDATE sh_prod_currency SET status=?, `updatedOn` =? WHERE status=? AND prodId= ?";
		$values = array("I", date("Y-m-d H:i:s"), "A", $prodId); 
		$result = $this->connection->prepare($sql);
		$this->connection->execute($result,$values);
		
		foreach($currency as $key => $value)
		{
			self::insertProdCurrency($prodId, self::getCurrencyIdByCode($key), $value);
		}
		return true;
	}
	
	
	
}
