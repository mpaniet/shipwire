<?php 
require_once ("connect.php");
require_once ("Info/UserInfo.php");

class ShUserDB
{
	public function  __construct()
	{
		$this->connection=Connect::get_connection();
	}
	
	public function  __destruct()
	{
		if($this->connection){
        	$this->connection->disconnect();
      	}
	}
	
	public function getUserInfo($userName)
	{
		$result = $this->connection->query("SELECT userId, emailId , password, FirstName, LastName FROM sh_user WHERE emailId =?", array($userName));
		$info = array();
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		if($result->numRows()==0)
			return null;
				
		while($result->fetchInto($userRow, DB_FETCHMODE_OBJECT))
		{
				$userInfo = new UserInfo($userRow->userId, $userRow->emailId, $userRow->password, $userRow->FirstName, $userRow->LastName);
				array_push($info, $userInfo);
		}
		return $info;
	}
	
	public function insertUser($firstName, $lastName, $userName, $password)
	{
		$sql ="INSERT INTO sh_user(`FirstName`, `LastName`,`emailId`, `password`, `createdOn`) VALUES(?,?,?,?,?)";
		$values = array($firstName, $lastName, $userName, $password, date("Y-m-d H:i:s"));
		$result = $this->connection->query($sql,$values);
		if(DB::isError($result)) {
			throw new Exception($result->getDebugInfo());
		}
		$this->connection->execute($result);
	}
	
	
	
}
/*
$user = new ShUserDB();
$user->getUserInfo("pankaj@pankaj.com");*/