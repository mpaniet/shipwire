<?php
	$error = "";
	$msg = "";
	$fileElementName = 'csv_import';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['csv_import']['tmp_name']) || $_FILES['csv_import']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{

            $ext = explode(".",$_FILES['csv_import']['name']);  
            if($ext[1]!='csv')
            {
              $error="Please select a valid csv file!!";
            }   
            if( @filesize($_FILES['csv_import']['tmp_name'])==0)
            {
              $error="Empty File";
            } 
            $contents = file($_FILES['csv_import']['tmp_name']);
            if(count($contents) >0)
            {
    	        $prodName = null;
                $prodDesc = null;
                $width = null;
                $height = null;
                $length = null;
                $weight = null;
                $quantity = null;
                $inputCsvArray=array();
                $field_name = explode(",",$contents[0]);
                if(empty($field_name))
                {
                  $error ="No header found in file!";
                }
                else 
                {
                  for($i=0;$i<count($field_name);$i++)
                  {
                     if(strtolower(trim($field_name[$i]))=='prodname')
                       $prodName=$i;
                     elseif(strtolower(trim($field_name[$i]))=='proddesc')
                       $prodDesc=$i;
                     elseif(strtolower(trim($field_name[$i]))=='width')
                       $width=$i;
                     elseif(strtolower(trim($field_name[$i]))=='length')
                       $length=$i;
                     elseif(strtolower(trim($field_name[$i]))=='height')
                      $height=$i;
                     elseif(strtolower(trim($field_name[$i]))=='weight')
                      $weight=$i; 
                     elseif(strtolower(trim($field_name[$i]))=='quantity')
                      $quantity=$i; 
                  }
                  if(is_null($prodName) || is_null($prodDesc) || is_null($width) || is_null($length) || is_null($height) || is_null($weight) || is_null($quantity))
                  {
                    $error="Data cannot be imported without required headers!";
                  }
                  else
                  {
                    $k=0;
                    for($i=1;$i<count($contents);$i++)
                    {    
                        $csv_val  = explode(",",$contents[$i]);
                        if(count($csv_val)>1)
                        {
                        	$search                     = array("'");
                            $replace_by_entities_name   = array("#39;");
                            $inputCsvArray[$k]['prodName'] = str_replace($search,$replace_by_entities_name, (trim($csv_val[$prodName])));
                            $inputCsvArray[$k]['prodDesc'] = str_replace($search,$replace_by_entities_name,(trim($csv_val[$prodDesc])));
                            $inputCsvArray[$k]['width'] = trim($csv_val[$width]);
                            $inputCsvArray[$k]['height'] =trim($csv_val[$height]);
                            $inputCsvArray[$k]['length'] = trim($csv_val[$length]);
                            $inputCsvArray[$k]['weight'] = trim($csv_val[$weight]);
                            $inputCsvArray[$k]['qty'] = trim($csv_val[$quantity]);
                        	$k++;
                        }
                             
                    }   
                    $msg=json_encode($inputCsvArray);
                  }
                } 
            }
			@unlink($_FILES['csv_import']);		
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>
