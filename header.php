<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>ShipWire</title>
<link rel="stylesheet" href="../css/style.css" />
<script src="../js/jquery-2.1.0.min.js"></script>
<script src="../js/jquery.validate.min.js"></script>
<script src="../js/jquery.liveaddress.min.js"></script>
<script src="../js/additional-methods.js"></script>
<script src="../js/jquery.price_format.2.0.min.js"></script>
<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>