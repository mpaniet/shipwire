	<p>
	    <h1>INTRODUCTION</h1><br>
	    At Shipwire, we want to be sure our merchants have the best experience getting their information into our system. Merchants have a lot of data, one of which is their Product Catalog. We'd like to see how you envision a new merchant entering and submitting their product information into our application.
	    There are no limits as to the amount of time you spend on this or the amount of code you write, but try to keep it simple and presentable. If you need frameworks, libraries, databases, or other tools to write the application, please mention them in the documentation for your application.
	    Once you've finished your application, please send your results to your recruitment contact. Please share your code on github or bitbucket and provide a way to access and test your application.
    </p>
    <br>
     <?php
        if(!(isset($_SESSION) && isset($_SESSION['UUID'])))
        {?> 
        	<h2>Register/Login</h2><br>To enter production information.
		    <div class="wrapper">
		        <a href="login.php">Login/Register</a>
		    </div>
        <?php 
        }else {?>
        	<h2>Welcome <?php 
        	echo $_SESSION['firstName'];?>!!</h2><br>
		    <div class="wrapper">
		        <a href="products.php">View Products</a>
		    </div>
        <?php }
        ?>
    
    