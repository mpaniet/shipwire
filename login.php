<?php require_once("topNav.php");?>
<article id="grid">
    <div id="breadcrumb"><a href="/">Home</a></div>
    <header>
        &nbsp;
    </header>
    	<?php if(isset($_SESSION['UUID'])){?>
			<center>Already Logged In!!</center>
		  <?php }else{?>
		<article id="login" >
		   <div id = "existingUser"> 
				<form id="newUserForm" >
			    	<h1>Existing customers</h1>
			        <p><label for="email">Email</label>
			        <input type="email" name="loginUserName" id= "loginUserName" required maxlength="255"/></p>
			        <p><label for="pasword">Password</label>
			        <input type="password" required name="loginPwd" id="loginPwd" maxlength="255" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}"  onChange="this.setCustomValidity(this.validity.patternMismatch ? 'Password must contain at least 6 characters, including UPPER/lowercase and numbers' : '');"/></p>
			        <p><button id="submit" onClick="javascript:login();">Sign in</button> <a href="#">Forgotten password?</a></p>
			    </form>
		    </div>
		    <div id = "newUser" style="display:none;"> 
		        <form style="height:400px;Width:400px"  id="registerForm" >
		            <h1>New customers</h1>
		            <p><label for="firstName">First Name</label>
		            <input type="text" name="firstName"  id= "firstName" required maxlength="60" pattern="\w+" /></p>
		            <p><label for="lastName">Last Name</label>
		            <input type="text" name="lastName"  id= "lastName" required maxlength="60"  pattern="\w+" /></p>
		            <p><label for="email">Email</label>
		            <input type="email" name="userName" id= "userName" required maxlength="255"/></p>
		            <p><label for="pasword">Password <br>[Password must contain at least 6 characters, including UPPER/lowercase and numbers]</label>
		            <input type="password" name="pwd" id="pwd" required maxlength="255"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}" onChange="this.setCustomValidity(this.validity.patternMismatch ? 'Password must contain at least 6 characters, including UPPER/lowercase and numbers' : ''); if(this.checkValidity()) form.confPassword.pattern = this.value;"/></p>
		            <p><label for="confPassword">Confirm Password</label>
		            <input type="password" name="confPassword" id="confPassword"  required maxlength="255"  onChange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');"></p>
		            <p><button type="button" id="submit" onClick="javascript:register();">Register</button></p>
		        </form>
		    </div>
		    <section id="displaySection">
		    	<h2>New to Pankaj Corp?</h2>
		        <p><button type="button" onClick="javascript:display();">Continue</button></p>
		    </section>
		    </article>
		    <?php } ?>
		
		 <footer> &nbsp;</footer>
    </article>
    <?php require_once("footer.php");?>
</body>
</html>

<script>
  display = function()
  {
	  $("#existingUser").hide();
	  $("#displaySection").hide();
	  $("#newUser").show();
  }
  
  register = function()
  {
	  if($("#registerForm").valid())
	  {
		  var dataString = {};
		  var url = "/request.php?action=register";
		  dataString.firstName = $("#firstName").val();
		  dataString.lastName = $("#lastName").val();
		  dataString.userName = $("#userName").val();
		  dataString.password = $("#pwd").val();
		  dataString.confPassword = $("#confPassword").val();
		  $.post(url, dataString,
	              function(info) {
			  window.location = "/";
		  });
	  }
  }
  
  login = function()
  {
	  if($("#newUserForm").valid())
	  {
	      var dataString = {};
	      var url = "/request.php?action=login";
	      dataString.userName = $("#loginUserName").val();
	      dataString.password = $("#loginPwd").val();
	      $.post(url, dataString,
	              function(info) {
	          window.location = "/";
	      });
	  }
  }
  
  
</script>