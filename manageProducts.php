<?php require_once("topNav.php");?>
<script src="../js/ajaxfileupload.js" type="text/javascript"></script>
<?php require_once("validateSession.php")?>
	<div id="productsDisplay" >
		<article id="grid">
		    <div id="breadcrumb"><a href="/">Home</a>> <a href="manageProducts.php">Manage Products</a></div>
		    <header>
		    	<div id="productName">Manage Products&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="addNewProduct();">Add New Product</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onClick="uploadCsvProduct();">Upload Csv</a></div>
		    </header>
		    <article id="table">
			    <div id="display"></div>
			    <div id="uploadCsv" style="display:none;">
			    	<?php require_once("uploadCsv.php")?>
			    </div>
		    </article>
		</article>
    </div>
    <div id ="addNewProduct" style="display:none">
    <article id="login" style="clear:inherit;">
    		<div id="processing"  style="display:none">Processing....</div>
			<div id="placeHolder" >
				 <form style="height:900px;width:350px;margin-top:-400px"  id="productForm"  onSubmit="return false;">
		            <h1>Add/Edit Product</h1>
		            <input type="hidden" value="" id="prodId">
		            <p><label for="prodName">Product Name</label>
		            <input type="text" name="prodName"  id= "prodName" required maxlength="60"  /></p>
		            <p><label for="prodDesc">Product Description</label>
		            <textarea  name="prodDesc"  id= "prodDesc" rows="5" cols="40"  required /></textarea></p>
		            <p><label for="width">Width</label><input type="text" name="width" id= "width" required maxlength="4" style="width:30px" /></p>
		            <p><label for="length">Length</label><input type="text" name="length" id="length" required maxlength="4" style="width:30px" /></p>
		            <p><label for="height">Height</label><input type="text" name="height" id="height" required  maxlength="4" style="width:30px" /></p>
		            <p><label for="weight">Weight</label><input type="text" name="weight" id="weight" required  maxlength="4" style="width:30px"/></p>
		            <p><label for="qty">Quantity</label><input type="text" name="qty" id="qty" required  maxlength="4" style="width:30px"/></p></p>
		            <div id="dynPrice"></div>
		            <p><button type="submit" id="submitOrder">Add/Edit Product</button></p>
				  </form>
			</div>	
	    </article>
	 </div>   
    <?php require_once("footer.php");?>
    <script>
    			var currency =0;
       			$(document).ready(function() 
    			{
       					clearValues();
			   			validate();
			   			getAllproducts();
			   			currency = getCurrency();
				});

       			addNewProduct = function()
			    {
       			  clearValues();	
   				  $("#display").hide(); 	    
   				  $("#addNewProduct").show();
   				  $("#uploadCsv").hide();
   				  if(currency!= "undefined")
   				  {
   	   				  var curr="";
   	   				  for(var j=0; j< 4; j++)
   	   				  {
	   	   				  curr = curr + '<p><select id="crr_'+j+'" name="crr_'+j+'" required onChange="javascript:validateAlreadySelected(\''+j+'\');"><option  value="">Select</option>';
	   				  	  for(var i=0;i<currency.length>0;i++)
	   				  	  {
	   				  		curr = curr +'<option value="'+currency[i].currencyCode+'">'+currency[i].symbol+'</option>';
	   				  	  }
	   				  	  curr = curr +'</select>&nbsp;&nbsp;&nbsp;<input type="text" name="price_'+j+'" id="price_'+j+'" required  maxlength="10" style="width:80px" placeholder="Enter Price.."/></p>';
   	   				  }
   				  	  $("#dynPrice").html(curr);
   				  }	    
			   	}

       			validateAlreadySelected = function (j)
       			{
           			if(j>0)
           			{
	           			var oldValue = j-1;
	           			var currValue = j;
	           			var str = "crr_";
	           			if($("#crr_"+oldValue).val()==$("#crr_"+currValue).val())
	           			{
		           			alert("This value has already been selected. Please selectd different option!!");
		           			$("#crr_"+currValue).val("");
		           			return false;
	           			}
           			}
           			return;		
       			}
       			
       			getCurrency = function()
       			{
       				var dataString = {};
       				var url = "/request.php?action=getCurrency";
       				$.post(url, dataString,
			                 function(info) {
		                 		if(info=="null")
		                 		{
			                 		
		                 			currency='null';
		                 		}
		                 		else
		                 		{
		                 			currency = info;
		                 		}
			   	  		}, 'json');
       			}
       			
				function validate() 
				{
					$("#productForm").validate({
						rules: {
							prodName:{
								 required: true,
								 maxlength: 60
							},
							prodDesc:{
								required: true
							},
							width:{
								required: true,
								integer: true
							},
							height:{
								required: true,
								integer: true
							},
							weight:{
								required: true,
								integer: true
							},
							length:{
								required: true,
								integer: true
							},
							qty:{
								required: true,
								integer: true
							},
							price_0:{
								required: function() {
								     return ($("#price_0").val() != "" )},
								number: function() {
								     return ($("#price_0").val() != "" )}
							},
							price_1:{
								required: function() {
								     return ($("#price_1").val() != "" )},
								number: function() {
								     return ($("#price_1").val() != "" )}
							},
							price_2:{
								required: function() {
								     return ($("#price_2").val() != "" )},
								number: function() {
								     return ($("#price_2").val() != "" )}
							},
							price_3:{
								required: function() {
								     return ($("#price_3").val() != "" )},
								number: function() {
								     return ($("#price_3").val() != "" )}
							},
							crr_0:{
								required: function() {
								     return ($("#crr_0").val() != "" )}
							},
							crr_1:{
								required: function() {
								     return ($("#crr_1").val() != "" )}
							},
							crr_2:{
								required: function() {
								     return ($("#crr_2").val() != "" )}
							},
							crr_3:{
								required: function() {
								     return ($("#crr_3").val() != "" )}
							}
				        },
				        messages: {
				        	prodName: {
				                required: "Please enter valid Product Name"
				            },
				            prodDesc:{
				            	required: "Please enter valid product description!"
				            },
				            width:{
				            	required: "Please enter width of product",
				            	integer:"Only numeric values are allowed"
				            },
				            height:{
				            	required: "Please enter width of product",
				            	integer:"Only numeric values are allowed"
				            },
				            length:{
				            	required: "Please enter width of product",
				            	integer:"Only numeric values are allowed"
				            },
				            weight:{
				            	required: "Please enter width of product",
				            	integer:"Only numeric values are allowed"
				            },
				            qty: {
				                required: "Please enter valid quantity",
				                integer: "Please enter a quantity: (e.g. 1000 or 1999)"
				            },
				            price_0: {
				                required: "Please enter valid price",
				                number: "Please enter a valid price: (e.g. 100 or 1999.99)"
				            },
				            price_1: {
				                required: "Please enter valid price",
				                number: "Please enter a valid price: (e.g. 100 or 1999.99)"
				            },
				            price_2: {
				                required: "Please enter valid price",
				                number: "Please enter a valid price: (e.g. 100 or 1999.99)"
				            },
				            price_3: {
				                required: "Please enter valid price",
				                number: "Please enter a valid price: (e.g. 100 or 1999.99)"
				            },
				            curr_0: {
				                required: "Please select valid currency"
				            },
				            curr_1: {
				                required: "Please select valid currency"
				            },
				            curr_2: {
				                required: "Please select valid currency"
				            },
				            curr_3: {
				                required: "Please select valid currency"
				            },
				        },
				        submitHandler: function(form) {
				        	insertProduct();
				        },
					});
				}

				insertProduct = function()
				{
					if($("#productForm").valid())
					{
						var dataString = {};
						var priceArray = {};
						for(var i=0; i<4;i++)
						{
							if($("#price_"+i).val()!="")
							{
								var str = $("#crr_"+i).val();
								priceArray[str] = $("#price_"+i).val();
							}
						}
						var url ="";
						if($("#prodId").val() !='')
						{
							dataString.prodId = 	$("#prodId").val();
							url ="/request.php?action=updateProduct";
						}
						else
						{
							url = "/request.php?action=insertProduct";
						}
						dataString.userId= <?php echo $_SESSION['userId']?>;
						dataString.prodName = $("#prodName").val();
						dataString.prodDesc = $("#prodDesc").val();
						dataString.width = $("#width").val();
						dataString.height = $("#height").val();
						dataString.length = $("#length").val();
						dataString.weight = $("#weight").val();
						dataString.qty = $("#qty").val();
						dataString.currency =priceArray;
						$.post(url, dataString,
				                 function(info) {
			                 		if(info != null || info !="null")
			                 		{
				                 		window.location="/manageProducts.php";
			                 		}
						},'json');
					}
				}
				
				getAllproducts =  function()
				{
					  var dataString = {};
			    	  var display_str ="";
				   	  var url = "/request.php?action=getProducts";
				   	  $("#paging").hide();
				   	  $('#productDescription').hide();
	    		  	  $('#placeOrder').hide();
				   	  dataString.userId = <?php echo $_SESSION['userId']?>;
				   	  $.post(url, dataString,
				                 function(info) {
			                 		if(info=="null" || info ==null)
			                 		{
				                 		
				                 		display_str='<center><ul><li style="align:center">No Products Found!!!</li></ul></center>';
			                 		}
			                 		else
			                 		{
			                 			display_str= display_str + '<table style="border:3px;height:10px">';
			                 			display_str= display_str + '<tr style="border:3px"><th style="width:10%;height:10px">Product Id</th><th style="width:20%;height:10px">Name</th><th style="width:20%;height:10px">Description</th><th style="width:10%;height:10px">Currency</th><th style="width:10%;height:10px">';
			                 			display_str= display_str + 'Price</th><th style="width:10%;height:10px">Width</th><th style="width:10%;height:10px">Height</th><th style="width:10%;height:10px;height:10px">Weight</th><th style="width:10%;height:10px">Quantity</th><th style="width:30%;height:10px">Action</th></tr>';
				                 		for(var i=0; i<info.length; i++)
				                 		{
					                 		 var currencyInfo=  'Price & currency';
					                 		 if(info[i].currencyInfo != null)
					                 		 {
					                 			 currencyInfo=  '<table style="border:3px;height:10px">';
						                 		 for(var z =0; z< info[i].currencyInfo.length;z++ )
						                 		 {
						                 			currencyInfo = currencyInfo + '<tr><td style="height:10px">'+info[i].currencyInfo[z].currencyCode+'</td><td style="height:10px">'+info[i].currencyInfo[z].value+'</td></tr>';
						                 		 }
						                 		currencyInfo = currencyInfo +'</table>';	
											}
				                 			display_str= display_str + '<tr id="pr_'+info[i].prodId+'"><td style="height:10px">'+info[i].prodId+'</td><td style="height:10px">'+info[i].name+'</td><td style="height:10px">'+info[i].description.slice(0, 100)+'</td><td style="height:10px" colspan="2">'+currencyInfo+'</td>';
				                 			display_str= display_str + '<td style="height:10px">'+info[i].width+'</td><td style="height:10px">'+info[i].height+'</td><td style="height:10px">'+info[i].length+'</td><td style="height:10px">'+info[i].weight+'</td><td style="height:10px"><a href="javascript:void(0);" onClick="editProduct(\''+info[i].prodId+'\');">Edit</a> <a href="javascript:void(0);" onClick="deleteProduct(\''+info[i].prodId+'\',\''+info[i].name+'\');">Delete</a></td></tr>';
				                 		}
				                 		display_str= display_str + '</table>';
			                 		}
			                 		$("#display").html(display_str);
			                 		$('#display').show();
				   	  		}, 'json');
				}

				clearValues =  function()
				{
					$("#prodName").val("");
					$("#prodDesc").val("");
					$("#width").val("");
					$("#height").val("");
					$("#weight").val("");
					$("#length").val("");
					$("#qty").val("");
					$("#crr_0").val("");
					$("#crr_1").val("");
					$("#crr_2").val("");
					$("#crr_3").val("");
					$("#price_0").val("");
					$("#price_1").val("");
					$("#price_2").val("");
					$("#price_3").val("");
				}

				editProduct = function (prodId)
				{
					  $('#display').hide();
					  $("#addNewProduct").show();
					  var dataString = {};
			    	  var display_str ="";
				   	  var url = "/request.php?action=getProductDetailInfo";
				   	  dataString.prodId = prodId;
				   	  dataString.page = 0;
				   	  $.post(url, dataString,
				                 function(info) {
			                 		if(info=="null" || info==null)
			                 		{
				                 		
				                 		display_str='<center><ul><li style="align:center">No Product Found!!!</li></ul></center>';
			                 		}
			                 		else
			                 		{
			                 			$("#prodId").val(info.prodId);
			                 			$("#prodName").val(info.name);
			        					$("#prodDesc").val(info.description);
			        					$("#width").val(info.width);
			        					$("#height").val(info.height);
			        					$("#weight").val(info.weight);
			        					$("#length").val(info.length);
			        					$("#qty").val(info.quantity);
			        					 if(currency!= "undefined")
			        	   				  {
			        	   	   				  var curr="";
			        	   	   				  for(var j=0; j< 4; j++)
			        	   	   				  {
			        		   	   				  curr = curr + '<p><select id="crr_'+j+'" name="crr_'+j+'" required onChange="javascript:validateAlreadySelected(\''+j+'\');"><option  value="">Select</option>';
			        		   				  	  for(var i=0;i<currency.length;i++)
			        		   				  	  {
			        		   				  		curr = curr +'<option value="'+currency[i].currencyCode+'">'+currency[i].symbol+'</option>';
			        		   				  	  }
			        		   				  	  curr = curr +'</select>&nbsp;&nbsp;&nbsp;<input type="text" name="price_'+j+'" id="price_'+j+'" required  maxlength="10" style="width:80px" placeholder="Enter Price.."/></p>';
			        	   	   				  }
			        	   				  	  $("#dynPrice").html(curr);
			        	   				  	  $("#dynPrice").show();
			        	   				  }	
			        					if(info.currencyInfo !=null)
			        					{
				        					for(var z=0; z<info.currencyInfo.length; z++ )
				        					{
				        						$("#crr_"+z).val(info.currencyInfo[z].currencyCode);
				        						$("#price_"+z).val(info.currencyInfo[z].value);
				        					}
			        					}
			                 		}
				   	  		}, 'json');
				}

				deleteProduct = function(prodId, prodName)
				{
					if(confirm("Are you sure you want to delete '"+prodName+"'?"))
					{
						var dataString = {};
						var url = "/request.php?action=delProduct";
						dataString.prodId = prodId;
						$.post(url, dataString,
				                 function(info) {
			                 		$("#pr_"+prodId).hide();
						}, 'json');
					}
					else
					{
						return false;
					}
					
				}

				uploadCsvProduct = function()
				{
					 $("#display").hide();
					 $("#addNewProduct").hide();
					 $("#uploadCsv").show();
				}

    </script>
</body>
</html>
