<?php require_once("topNav.php");?>
<?php require_once("validateSession.php")?>
	<div id="productsDisplay" >
		<article id="grid">
		    <div id="breadcrumb"><a href="/">Home</a>> <a href="products.php">Products</a></div>
		    <header>
		    	<div id="productName">Product Listing</div>
		    </header>
		    <?php if(isset($_GET['prodId']))
		    {?>
	    		<div id="productDescription" >Product Description</div>
	    	<?php }else{?>	
		    <div id="display"></div>
		    <?php }?>
	    </article>
    </div>
    <div id ="placeOrder" >
    <article id="login" style="clear:inherit;">
    		<div id="processing"  style="display:none">Processing....</div>
			<div id="placeHolder" >
				 <form style="height:580px;width:350px;margin-top:-300px"  id="orderForm" >
		            <h1>Place Order</h1>
		            <input type="hidden" value="" id="prodId">
		            <input type="hidden" value="" id="prodQuantity">
		            <input type="hidden" value="" id="prodPrice">
		            <p><label for="rcptName">Recipient Name</label>
		            <input type="text" name="rcptName"  id= "rcptName" required maxlength="60"  /></p>
		            <p><label for="strtAddress">Street Address</label>
		            <input type="text" name="strtAddress"  id= "strtAddress" required maxlength="255"  /></p>
		            <p><label for="city">City</label>
		            <input type="text" name="city" id= "city" required maxlength="60" /></p>
		            <p><label for="state">State</label>
		            <input type="text" name="state" id="state" required maxlength="60"  /></p>
		            <p><label for="zip">zipCode</label>
		            <input type="text" name="zip" id="zip" required  maxlength="15" /></p>
		            <p><label for="uphone">Phone</label>
		            <input type="text" name="uphone" id="uphone"  /></p>
		            <lable id="qtyError" class="error"></lable>
		            <p><label for="qty">Quantity <span id="qtyAvailable" style="display: none"></span></label>
		            <input type="text" name="qty" id="qty" required  maxlength="4" /></p>
		            <p><button type="submit" id="submitOrder"  >Place Order</button></p>
				  </form>
			</div>	
	    </article>
	 </div>   
    <?php require_once("footer.php");?>
    <script>
       			$(document).ready(function() 
    			{
       				<?php if(isset($_GET['prodId']))
       			    {?>
       					showProductInfo();
			   			validate();
			   		<?php }else{?>
			   			getAllproducts();
			   		<?php } ?>		 			   				
				});

	    		showProductInfo = function()
			    {
				     
		    	  var dataString = {};
		    	  var display_str ="";
			   	  var url = "/request.php?action=getProductDetailInfo";
			   	  $("#paging").hide();
			   	  dataString.prodId = <?php echo  isset($_GET['prodId'])?  $_GET['prodId']: "0"; ?>;
			   	  dataString.page = 0;
			   	  $.post(url, dataString,
			                 function(info) {
		                 		if(info=="null" || info==null)
		                 		{
			                 		
			                 		display_str='<center><ul><li style="align:center">No Product Found!!!</li></ul></center>';
		                 		}
		                 		else
		                 		{
		                 			$("#prodId").val(info.prodId);
		                 			$("#prodQuantity").val(info.quantity);
		                 			$("#prodPrice").val(info.value);
		                 			$("#qtyAvailable").html("Available Quantity:"+info.quantity);
		                 			display_str='<div id="description">';
			                 		display_str= display_str + '<h1>'+info.name+'</h1>';
			                 		if(info.currencyInfo != null)
			                 		{
			                 			for(var z=0; z< info.currencyInfo.length; z++)
			                 			{
			                 				display_str= display_str + '<BR><strong id="price">'+info.currencyInfo[z].symbol+" "+info.currencyInfo[z].value+'</strong><BR>';
			                 			}
			                 		}
			                 		display_str= display_str + '<p>'+info.description+'</p>';
			                 		display_str= display_str + '</div>';
		                 		}
		                 		$("#productDescription").html(display_str);
		                 		$("#qtyAvailable").show();
		                 		$('#productDescription').show();
			   	  		}, 'json');
			   	}
			   	
	    		<?php if(isset($_GET['prodId']))
   			    {?>
		    		var liveaddress = $.LiveAddress({
						key: "1557255155314564937", 
						debug: false, 
						autoVerify: true, 
						invalidMessage: "Pleaes enter a valid US Address!",
						addresses: [{ 
									  id: 'products',	
									  street: '#strtAddress', 
									  city: '#city', 
									  state: '#state', 
									  zipcode: '#zip', 
									  country: '#country' 
								}]
					});
	    		<?php }?>
				
				function validate() 
				{
					$("#orderForm").validate({
						rules: {
							rcptName:{
								 required: true,
								 maxlength: 60
							},
							strtAddress:{
								required: true,
								maxlength:255
							},
							city:{
								required: true,
								maxlength:60
							},
							state:{
								required: true,
								maxlength:60
							},
							zip:{
								required: true,
								maxlength:15,
								zipcodeUS:true
							},
							uphone: {
				                required: true,
				                minlength: 10,
				                maxlength: 14,
				                phoneUS: true
				            },
							qty:{
								required: true,
								integer: true
							}
				        },
				        messages: {
				        	rcptName: {
				                required: "Please enter valid Recipient Name"
				            },
				            strtAddress:{
				            	required: "Please enter valid US Street Address"
				            },
				            city:{
				            	required: "Please enter valid US city"
				            },
				            state:{
				            	required: "Please enter valid US state"
				            },
				            zip:{
				            	required: "Please enter valid US zip",
				            	zipcodeUS: "Please enter valid US zip code"
				            },
				        	uphone: {
				                required: "Please enter your phone number",
				                phoneUS: "Please enter a valid phone number: (e.g. 19999999999 or 9999999999)"
				            },
				            qty: {
				                required: "Please enter valid quantity",
				                integer: "Please enter a quantity: (e.g. 1000 or 1999)"
				            }
				        },
				        submitHandler: function(form) {
				        	placeOrder();
				        },
					});
				}
		
				placeOrder =  function()
				{
					if(!$("#orderForm").valid())
					{
						return false;
					}
					if($("#qty").val() > $("#prodQuantity").val())	
					{
						$("#qtyError").html("Quantity should not exceed the avaliable quantity");
						$("#qtyError").error();
					}
					var dataString = {};
					 $("#processing").html("Processing");
					var url = "/request.php?action=placeOrder";
					dataString.userId = <?php echo $_SESSION['userId']?>;
					dataString.prodId = $("#prodId").val();
					dataString.rcptName = $("#rcptName").val();
					dataString.strtAddress = $("#strtAddress").val();
					dataString.city = $("#city").val();
					dataString.state = $("#state").val();
					dataString.quantity = $("#qty").val();
					dataString.phone = $("#uphone").val();
					dataString.price = $("#prodPrice").val();
					dataString.zip = $("#zip").val();
					 $.post(url, dataString,
			                 function(info) {
							 clearValues();
			                 $("#placeHolder").hide();
			                 $("#processing").show();
			                 if(info=="true" || info==true)
			                 {
			                	 $("#processing").html("Order placed successfully!!");
			                 }
					 },'json');
				}

				getAllproducts =  function()
				{
					  var dataString = {};
			    	  var display_str ="";
				   	  var url = "/request.php?action=getProducts";
				   	  $("#paging").hide();
				   	  $('#productDescription').hide();
	    		  	  $('#placeOrder').hide();
				   	  dataString.userId = <?php echo $_SESSION['userId']?>;
				   	  dataString.page = 0;
				   	  $.post(url, dataString,
				                 function(info) {
			                 		if(info=="null" || info==null)
			                 		{
				                 		
				                 		display_str='<center><ul><li style="align:center">No Products Found!!! </li></ul></center>';
			                 		}
			                 		else
			                 		{
			                 			display_str='<ul id="items">';
				                 		for(var i=0; i<info.length; i++)
				                 		{
				                 			var price = "";
			                 				var currency = "";
				                 			if(info[i].currencyInfo != null)
					                 		{
					                 			for(var z=0; z< info[i].currencyInfo.length; z++)
					                 			{
					                 				price= info[i].currencyInfo[0].value;
					                 				currency= info[i].currencyInfo[0].symbol;
					                 				break;
					                 			}
					                 		}
				                 			display_str= display_str +'<li><a href="products.php?prodId='+info[i].prodId+'" class="title"><b>'+info[i].name+'</b></a>'+info[i].description.slice(0, 100)+'...<strong>'+currency+' '+price+'</strong><a href="products.php?prodId='+info[i].prodId+'">Click to Order...</a>';
				                 		}
				                 		display_str= display_str + '</ul>';
			                 		}
			                 		$("#display").html(display_str);
			                 		$('#display').show();
				   	  		}, 'json');
				}

				clearValues =  function()
				{
					$("#prodId").val("");
					$("#rcptName").val("");
					$("#strtAddress").val("");
					$("#city").val("");
					$("#state").val("");
					$("#qty").val("");
					$("#uphone").val("");
					$("#prodPrice").val("");
					$("#zip").val("");
				}
		
    </script>
</body>
</html>
