<?php
require_once './Common/common.php';

$params = array_merge($_GET, $_POST, $_FILES);
$cmn = new Common();
$response  = $cmn->processRequest($params);
$cmn->logError("Request.php::Response::".print_r(json_encode($response),1));
echo json_encode($response);
?>
