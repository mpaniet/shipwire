<?php require_once "header.php"?>
<header>
	<div class="wrapper" style="valign:top">
        <h1><a href="/" id="brand" title="Pankaj Malhotra">Pankaj Malhotra</a></h1>
         <?php 
         if((isset($_SESSION) && isset($_SESSION['UUID'])))
         {?>
        	<nav>
	            <ul>
	                <li>
	                  <a href="#">My Account</a>
	                  <ul class="sub-menu">
	                        <li><a href="manageProducts.php">Manage Products</a></li>
	                        <li><a href="products.php">View Product</a></li>
	                        <li><a href="#">View Orders</a></li>
	                    </ul>
	                </li>
	             </ul>
            </nav>
         <?php }?>       
    </div>
</header>
<aside id="top">
	<div class="wrapper">
        <div id="action-bar">
        <?php
        if(!(isset($_SESSION) && isset($_SESSION['UUID'])))
        {?> 
        	<p><button type="button" id="login" onClick="javascript:logIn();">Login/Register</button></p>
        <?php 
        }else {?>
        	<p><button type="button" id="logout" onClick="javascript:logOut();">LogOut</button></p>
        <?php }
        ?>
        </div>
    </div>
</aside>

<script>

logOut = function()
  {
	  var dataString = {};
	  var url = "/request.php?action=logout";
	  $.post(url, dataString,
              function(info) {
		  window.location = "/";
	  });
  }

logIn = function()
{
	window.location = "/login.php";
}
</script>