					<table> 
                        <img id="loading" src="./images/loading.gif" style="display:none;">
		                       <form id="importCsvFrm" name="importCsvFrm"  method="POST"  enctype="multipart/form-data">                
		                         <tr >                                   
		                            <th  colspan="2" style="height:10px;border:0px">Import Csv </th>
		                            <input type="hidden" id="importvalues" name="importvalues">
		                        </tr>
		                         <tr>
                                   <td colspan="2"  style="height:10px;border:0px">&nbsp;</td>
                                </tr> 
                                <tr>
                                  <td  style="height:10px;border:0px">
                                  <span>Your CSV file (comma separated values) must have the following headers:
										<p>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ProdName</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ProdDesc</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>width</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>length</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>height</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>weight</strong> - Required<br />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>quantity</strong> - Required<br />
										</p>
									</span>
                                  </td>
                                </tr>
                                <tr>
                                  <th style="height:10px;border:0px">CSV File</th>
                                  <td  style="height:10px;border:0px"><input type="file" name="csv_import" id="csv_import"></td>
                                </tr>
		                        <tr>
		                           <td colspan="2" style="height:10px;border:0px">&nbsp;</td>
		                        </tr> 
		                        <tr>
		                           <td colspan="2" style="height:10px;border:0px"><input type="button" onclick="javascript:return ajaxFileUpload();" value="Import Product"></td>
                        </tr>
                        </form>
                    </table>
<script>
var importCsvData=null;
function ajaxFileUpload()
{
        $("#loading")
        .ajaxStart(function(){
                $(this).show();
        })
        .ajaxComplete(function(){
                $(this).hide();
        });

        $.ajaxFileUpload
        (
                {
                        url:'doajaxfileupload.php',
                        secureuri:false,
                        fileElementId:'csv_import',
                        dataType: 'json',
                        data:{name:'logan', id:'id'},
                        success: function (data, status)
                        {
                                if(typeof(data.error) != 'undefined')
                                {
                                        if(data.error != '')
                                        {
                                                alert(data.error);
                                        }else
                                        {
                                      	  var parsedVal = JSON.parse(data.msg);
                                     	 	  if(parsedVal.length == 0)
                                               {
                                                  $('#uploadCsv').html('<table  ><tr ><td tyle="height:10px;border:0px"><B>No Record Found</B></td></tr></table>')
                                               }
                                               else
                                               {

                                              	    importCsvData = parsedVal;
                                                    var display_str = '';
                                                    display_str = display_str + '<table  >';
                                                    display_str = display_str + '<tr>';
                                                    display_str = display_str + '    <td colspan="7" style="height:10px;border:0px">&nbsp;</td>';
                                                    display_str = display_str + '</tr>';
                                                    display_str = display_str + '<tr >'; 
                                                    display_str = display_str + '<th style="height:10px;border:0px;border:0px">Prod Name></th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px;border:0px">Prod Desc</th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px;border:0px">Width</th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px">length</th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px">Height</th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px">Weight</th>';
                                                    display_str = display_str + '<th style="height:10px;border:0px">Quantity</th>';
                                                    display_str = display_str + '</tr> '; 
                                                    
                                                    for(i=0;i<parsedVal.length;i++)
                                                    {  
                                                  	  if(i%2==0) 
                                                        {                
                                                              display_str = display_str + '<tr >';
                                                        }
                                                        else
                                                        {
                                                            display_str = display_str + '<tr >';
                                                        }
                                                        display_str = display_str + '    <td style="height:10px;border:0px;">'+parsedVal[i].prodName.replace(/#39;/g, "\'")+'</td>';
                                                        display_str = display_str + '    <td style="height:10px;border:0px;">'+parsedVal[i].prodDesc.replace(/#39;/g, "\'")+'</td>';
                                                        display_str = display_str + '     <td style="height:10px;border:0px">'+parsedVal[i].width+'</td>';
                                                        display_str = display_str + '     <td style="height:10px;border:0px;">'+parsedVal[i].length+'</td>';
                                                        display_str = display_str + '     <td style="height:10px;border:0px;">'+parsedVal[i].height+'</td>';
                                                        display_str = display_str + '     <td style="height:10px;border:0px;">'+parsedVal[i].weight+'</td>';
                                                        display_str = display_str + '     <td style="height:10px;border:0px">'+parsedVal[i].qty+'</td>';
                                                        display_str = display_str + '</tr> '; 
                                                    }
                                                    display_str = display_str + '<tr>';
                                                    display_str = display_str + '    <td colspan="7" style="height:10px;border:0px">&nbsp;</td>';
                                                    display_str = display_str + '</tr>';
                                                    display_str = display_str + '<tr>';
                                                    display_str = display_str + '    <td colspan="7" style="height:10px;border:0px"><input type="button" onclick="javascript:return addAllProducts();" value="Add Products"></td>';
                                                    display_str = display_str + '</tr>';
                                                    display_str = display_str + '<tr>';
                                                    display_str = display_str + '    <td colspan="7" style="height:10px;border:0px">&nbsp;</td>';
                                                    display_str = display_str + '</tr>';
                                                    display_str = display_str + '</table>';
                                                   $('#uploadCsv').html(display_str);   
                                                   $('#uploadCsv').show();
                                               }  
                                               
                                        }
                                }
                        },
                        error: function (data, status, e)
                        {
							alert("Remove any comma's or spaces from the fields in the CSV");
                        }
                }
        )
        
        return false;

}

addAllProducts = function()
{
	if($("#productForm").valid())
	{
		var dataString = {};
		var url ="";
		url = "/request.php?action=insertCsvProduct";
		dataString.userId= <?php echo $_SESSION['userId']?>;
		dataString.info =importCsvData;
		$.post(url, dataString,
                 function(info) {
             		if(info != null || info !="null")
             		{
                 		window.location="/manageProducts.php";
             		}
		},'json');
	}
}
</script>